set number
syntax on
set cursorline
set cursorcolumn
set mouse=a
set autoindent
set ignorecase
set hlsearch
set tabstop=4
let g:vim_markdown_folding_disabled = 1

filetype plugin on
if has('gui_running')
	set grepprg=grep\ -nH\ $*
	filetype indent on
	let g:tex_flavor='latex'
endif
